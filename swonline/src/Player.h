#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <iostream>

class Player {
public:
    Player();
    ~Player();

    std::wstring username;
    std::string texture_path;

    int32_t x;
    int32_t y;
    int32_t width;
    int32_t height;
    int32_t tex_x;
    int32_t tex_y;
    int32_t tex_width;
    int32_t tex_height;
    bool horizontal_flip = false;
};

#endif
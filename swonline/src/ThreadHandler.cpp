#include "ThreadHandler.h"

ThreadHandler::ThreadHandler() {

}

ThreadHandler::~ThreadHandler() {

}

void ThreadHandler::init(std::unique_ptr<Server> server) {
    this->shared_thread_handler.server = std::move(server);
    this->initialized = true;
}

void* threadAcceptingConnection(void* arg) {
    SharedThreadHandler* shared = (SharedThreadHandler*)arg;

    while (true) {
        std::cout << "Waiting connection..." << std::endl;
        int32_t id = shared->server->getServerId();
        shared->pushSocketID(id);
        sleep(1);
    }

    std::cout << "Finish thread of connections" << std::endl;
}

void ThreadHandler::startAcceptingConnections() {
    assert(this->initialized);

    pthread_t thread;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    int32_t res = pthread_create(
        &thread,
        &attr,
        threadAcceptingConnection,
        (void*)&this->shared_thread_handler
    );

    if (res) {
        std::cout << "Fail to create thread accept connections" << std::endl;
        assert(false);
    }
}

void* threadReceivingMessages(void* arg) {
    SharedThreadReceiving* shared_recv = (SharedThreadReceiving*)arg;
    
    int32_t socket_id = shared_recv->socket_id;
    SharedThreadHandler* shared = shared_recv->shared_thread_handler;

    delete shared_recv;

    std::vector<uint8_t> buffer(BUFFER_SIZE);
    while (true) {
        std::cout << "receiving message from " << socket_id << std::endl;

        std::unique_ptr<WrapMessage> wmessage = std::make_unique<WrapMessage>(
            shared->server->receiveMessage(socket_id, buffer),
            socket_id
        );

        // if disconected
        if (wmessage->message == nullptr) {
            shared->disconnected(socket_id);
            break;
        }

        shared->pushMessages(std::move(wmessage));
    }

    std::cout << "user " << socket_id << " disconnected" << std::endl;
}

void ThreadHandler::startReceivingMessages() {
    assert(this->initialized);

    sleep(2); // how much time? I don't know TODO

    std::vector<int32_t> current_socket_ids;
    this->shared_thread_handler.getSocketIDs(&current_socket_ids);
    std::cout
        << "Current users online: "
        << current_socket_ids.size()
        << std::endl;

    // Get difference between already logged and new ones
    std::vector<int32_t> new_socket_ids;
    std::set_difference(
        current_socket_ids.begin(),
        current_socket_ids.end(),
        this->running_socket_ids.begin(),
        this->running_socket_ids.end(),
        std::inserter(new_socket_ids, new_socket_ids.begin())
    );
    std::cout
        << "New users: "
        << new_socket_ids.size()
        << std::endl;

    this->running_socket_ids = current_socket_ids;

    for (int32_t socket_id : new_socket_ids) {
        SharedThreadReceiving* shared_recv = new SharedThreadReceiving();
        shared_recv->shared_thread_handler = &this->shared_thread_handler;
        shared_recv->socket_id = socket_id;

        pthread_t thread;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

        int32_t res = pthread_create(
            &thread,
            &attr,
            threadReceivingMessages,
            (void*)shared_recv
        );

        if (res) {
            std::cout
                << "Fail to create thread receive messages in socket "
                << socket_id
                << std::endl;
            assert(false);
        }
    }
}

void ThreadHandler::getMessages(std::vector<std::unique_ptr<WrapMessage> >& out) {
    assert(this->initialized);
    this->shared_thread_handler.getMessages(&out);
}

Server* ThreadHandler::getServer() {
    return this->shared_thread_handler.server.get();
}

void ThreadHandler::getSocketIDs(std::vector<int32_t>* out) {
    this->shared_thread_handler.getSocketIDs(out);
}

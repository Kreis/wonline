#include "Authentication.h"


Authentication::Authentication() {

}

Authentication::~Authentication() {

}

void Authentication::updateOnlinePlayers(std::vector<int32_t>& socket_ids) {
    std::vector<int32_t> new_socket_ids;
    std::vector<int32_t> deprecated_socket_ids;

    // Calculate new socket_ids
    std::set_difference(
        socket_ids.begin(),
        socket_ids.end(),
        this->socket_ids.begin(),
        this->socket_ids.end(),
        std::inserter(new_socket_ids, new_socket_ids.begin())
    );

    // Calculate deprecated sockets_ids
    std::set_difference(
        this->socket_ids.begin(),
        this->socket_ids.end(),
        socket_ids.begin(),
        socket_ids.end(),
        std::inserter(deprecated_socket_ids, deprecated_socket_ids.begin())
    );

    // Push new sockets
    for (int32_t id : new_socket_ids) {
        this->socket_ids.push_back(id);
        this->players.push_back(nullptr);
        this->authenticated.push_back(false);
    }

    // Remove deprecated sockets
    int32_t last_id = this->socket_ids.size() - 1;
    for (int32_t id : deprecated_socket_ids) {
        for (int32_t i = 0; i <= last_id; i++) {
            if (this->socket_ids[ i ] == id) {
                std::swap(
                    this->socket_ids[ i ],
                    this->socket_ids[ last_id ]
                );
                std::swap(
                    this->authenticated[ i ],
                    this->authenticated[ last_id ]
                );
                std::swap(
                    this->players[ i ],
                    this->players[ last_id ]
                );
                last_id--;
                break;
            }
        }
    }
    this->socket_ids.resize(last_id + 1);
    this->authenticated.resize(last_id + 1);
    this->players.resize(last_id + 1);
}

bool Authentication::login(int32_t socket_id, std::wstring username) {

    std::wcout << L"authorizing username " << username << std::endl;
    int32_t i;
    for (i = 0; i < this->socket_ids.size(); i++) {
        if (this->socket_ids[ i ] == socket_id) {
            break;
        }
    }
    
    // if player break connection before process login
    if (i == this->socket_ids.size()) {
        return false;
    }

    if (username == L"yceraf_5454") {
           this->players[ i ] = std::make_unique<Player>();
           this->players[ i ]->username = username;
           this->players[ i ]->texture_path = "textures/ph-player-128.png";
           this->players[ i ]->x = 2432;
           this->players[ i ]->y = 2112;
           this->players[ i ]->width = 128;
           this->players[ i ]->height = 128;
           this->players[ i ]->tex_x = 0;
           this->players[ i ]->tex_y = 0;
           this->players[ i ]->tex_width = 128;
           this->players[ i ]->tex_height = 128;

           this->authenticated[ i ] = true;
    } else if (username == L"zeres.ia") {
           this->players[ i ] = std::make_unique<Player>();
           this->players[ i ]->username = username;
           this->players[ i ]->texture_path = "textures/colors.png";
           this->players[ i ]->x = 2100;
           this->players[ i ]->y = 2112;
           this->players[ i ]->width = 128;
           this->players[ i ]->height = 128;
           this->players[ i ]->tex_x = 0;
           this->players[ i ]->tex_y = 0;
           this->players[ i ]->tex_width = 128;
           this->players[ i ]->tex_height = 128;

           this->authenticated[ i ] = true;
    } else {
        return false;
    }

    return true;
}

bool Authentication::isLogged(int32_t socket_id) {
    int32_t i;
    for (i = 0; i < this->socket_ids.size(); i++) {
        if (this->socket_ids[ i ] == socket_id) {
            break;
        }
    }
    
    if (i == this->socket_ids.size()) {
        return false;
    }

    return this->authenticated[ i ];
}

bool Authentication::isLogged(std::wstring username) {
    int32_t i;
    for (i = 0; i < this->players.size(); i++) {
        if (this->players[ i ]->username == username) {
            break;
        }
    }
    
    if (i == this->players.size()) {
        return false;
    }

    return this->authenticated[ i ];
}

Player* Authentication::getPlayerBySocket(int32_t socket_id) {
    int32_t i;
    for (i = 0; i < this->socket_ids.size(); i++) {
        if (this->socket_ids[ i ] == socket_id) {
            break;
        }
    }
    
    // if player break connection before process login
    if (i == this->socket_ids.size()) {
        return nullptr;
    }

    return this->players[ i ].get();
}

#ifndef _LOGIC_H_
#define _LOGIC_H_

#include <vector>
#include <memory>
#include "Server.h"
#include "WrapMessage.h"
#include "Authentication.h"

class Logic {
public:
    Logic();
    ~Logic();

    void update(
        std::vector<std::unique_ptr<WrapMessage> >& messages,
        Server* server,
        std::vector<int32_t>& socket_ids
    );

private:
    Authentication auth;
    StateMessage state;
};

#endif
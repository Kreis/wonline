#include "WrapMessage.h"

WrapMessage::WrapMessage(
    std::unique_ptr<Message> message,
    int32_t socket_id
): message(std::move(message)), socket_id(socket_id) {

}

WrapMessage::~WrapMessage() {
    
}
#ifndef _WRAP_MESSAGE_H_
#define _WRAP_MESSAGE_H_

#include <iostream>
#include <memory>
#include "../../mwonline/LoginMessage.h"

class WrapMessage {
public:
    WrapMessage(std::unique_ptr<Message> message, int32_t socket_id);
    ~WrapMessage();

    std::unique_ptr<Message> message;
    int32_t socket_id;
};

#endif

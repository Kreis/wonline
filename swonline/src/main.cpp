#include <string.h>
#include <vector>
#include <iostream>
#include "Server.h"
#include "ThreadHandler.h"
#include "Logic.h"

int main(int argc, char const *argv[]) {

    if (argc <= 1) {
        std::cout << "Bad arguments number" << std::endl;
        return 0;
    }

    std::unique_ptr<Server> server = std::make_unique<Server>();
    int32_t port = atoi(argv[ 1 ]);
    server->init(port);
    std::cout << "Started server" << std::endl;

    // Start thread to accept connections
    ThreadHandler thread_handler;
    thread_handler.init(std::move(server));
    thread_handler.startAcceptingConnections();

    Logic logic;
    while (true) {
        // Start thread for new users to receive messages
        thread_handler.startReceivingMessages();

        // Get received messages
        std::vector<std::unique_ptr<WrapMessage> > messages;
        thread_handler.getMessages(messages);

        // Get all sockets running
        std::vector<int32_t> socket_ids;
        thread_handler.getSocketIDs(&socket_ids);

        // Update logic and send the corresponding messages
        logic.update(
            messages, thread_handler.getServer(), socket_ids
        );
    }

    return 0;
}

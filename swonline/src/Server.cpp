#include "Server.h"

Server::Server() {

}

Server::~Server() {
    
}

void Server::init(int32_t port) {
    
    int32_t opt = 1;
    this->addrlen = sizeof(this->address);
       
    // Creating socket file descriptor
    this->server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd == 0) { 
        perror("socket failed");
        exit(EXIT_FAILURE);
    } 

    if (
            setsockopt(
                server_fd,
                SOL_SOCKET,
                SO_REUSEADDR | SO_REUSEPORT,
                &opt,
                sizeof(opt)
            )
    ) { 
        perror("setsockopt");
        exit(EXIT_FAILURE);
    } 
    this->address.sin_family = AF_INET;
    this->address.sin_addr.s_addr = INADDR_ANY;
    this->address.sin_port = htons(port);

    if (
            bind(
                server_fd,
                (struct sockaddr*)&this->address,
                sizeof(this->address)
            ) < 0
    ) { 
        perror("bind failed");
        exit(EXIT_FAILURE);
    } 
    if (
            listen(server_fd, 3) < 0
    ) { 
        perror("listen");
        exit(EXIT_FAILURE);
    }

    this->initialized = true;
}

const int32_t Server::getServerId() {
    assert(this->initialized);

    // Create new socket
    int32_t new_socket = accept(
        this->server_fd,
        (struct sockaddr*)&this->address,
        (socklen_t*)&this->addrlen
    );
    if (new_socket < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    this->initialized = true;

    return new_socket;
}

std::unique_ptr<Message> Server::receiveMessage(
    int32_t socket_id,
    std::vector<uint8_t>& buffer
) {
    int32_t readed  = 0;
    while (readed < 4) {
        int32_t val_read = read(
            socket_id,
            &buffer[ readed ],
            buffer.size()
        );

        if (val_read == -1) {
            std::cout << "ERROR receiving request" << std::endl;
            return nullptr;
        }

        if (val_read == 0) {
            return nullptr;
        }

        readed += val_read;
    }
    int32_t size_data =
        (buffer[ 0 ] << 24) |
        (buffer[ 1 ] << 16) |
        (buffer[ 2 ] << 8) |
        (buffer[ 3 ] << 0);
    
    while (readed < 4 + size_data) {
        int32_t val_read = read(
            socket_id,
            &buffer[ readed ],
            buffer.size()
        );

        if (val_read == -1) {
            std::cout << "ERROR receiving request" << std::endl;
            return nullptr;
        }

        if (val_read == 0) {
            return nullptr;
        }

        readed += val_read;
    }
    
    std::cout << "received " << readed << " bytes" << std::endl;

    return std::move(
        MessageCreator::createMessage(
            std::vector<uint8_t>(
                buffer.begin() + 4,
                buffer.begin() + 4 + size_data
            )
        )
    );
}

void Server::sendMessage(
    int32_t socket_id,
    Message* message
) {
    std::vector<uint8_t> data = message->getBytes();
    int32_t data_size = data.size();
    data.insert(
        data.begin(),
        {
            (uint8_t)((data_size >> 24) & 0xFF),
            (uint8_t)((data_size >> 16) & 0xFF),
            (uint8_t)((data_size >> 8) & 0xFF),
            (uint8_t)((data_size >> 0) & 0xFF),
        }
    );

    int32_t val_send = send(socket_id, &data[0], data.size(), 0);
    std::cout << "sent " << val_send << " bytes" << std::endl;
}

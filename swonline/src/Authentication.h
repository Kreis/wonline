#ifndef _AUTHENTICATION_H_
#define _AUTHENTICATION_H_

#include <vector>
#include <memory>
#include <algorithm>
#include "Player.h"

class Authentication {
public:
    Authentication();
    ~Authentication();

    void updateOnlinePlayers(std::vector<int32_t>& socket_ids);
    bool login(int32_t socket_id, std::wstring username);

    bool isLogged(int32_t socket_id);
    bool isLogged(std::wstring username);
    Player* getPlayerBySocket(int32_t socket_id);

private:
    std::vector<int32_t> socket_ids;
    std::vector<std::unique_ptr<Player> > players;
    std::vector<bool> authenticated;
};

#endif
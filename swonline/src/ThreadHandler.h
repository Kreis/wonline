#ifndef _THREAD_HANDLER_H_
#define _THREAD_HANDLER_H_

#include <pthread.h>
#include <vector>
#include <memory>
#include <algorithm>
#include "Server.h"
#include "SharedThreadHandler.h"
#include "WrapMessage.h"

#define BUFFER_SIZE 2048

class ThreadHandler {
public:
    ThreadHandler();
    ~ThreadHandler();

    void init(std::unique_ptr<Server> server);

    void startAcceptingConnections();
    void startReceivingMessages();

    void getMessages(std::vector<std::unique_ptr<WrapMessage> >& out);

    Server* getServer();
    void getSocketIDs(std::vector<int32_t>* out);

private:
    bool initialized = false;
    SharedThreadHandler shared_thread_handler;
    std::vector<int32_t> running_socket_ids;
};

#endif

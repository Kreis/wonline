#include "Logic.h"

Logic::Logic() {

}

Logic::~Logic() {

}

void Logic::update(
    std::vector<std::unique_ptr<WrapMessage> >& messages,
    Server* server,
    std::vector<int32_t>& socket_ids
) {

    this->auth.updateOnlinePlayers(socket_ids);

    for (std::unique_ptr<WrapMessage>& wmessage : messages) {

        std::cout << "arrive new message" << std::endl;
        if (wmessage->message->getTypeID() == LoginMessage::typeID) {
            std::cout << "identified login message" << std::endl;
            LoginMessage* login_message = (LoginMessage*)wmessage->message.get();
            bool logged = this->auth.login(
                wmessage->socket_id,
                login_message->getUsername()
            );

            if (logged) {
                std::cout << "is Logged succesfully" << std::endl;
            }
        }

        if ( ! this->auth.isLogged(wmessage->socket_id)) {
            continue;
        }

        Player* player = this->auth.getPlayerBySocket(wmessage->socket_id);

        if (player == nullptr) {
            continue;
        }

        std::vector<std::unique_ptr<PlayerData> >* v = this->state.getPlayersData();
        int32_t i;
        for (i = 0; i < v->size(); i++) {
            if (v->at(i)->username == player->username) {
                break;
            }
        }
        if (i == v->size()) {
            std::unique_ptr<PlayerData> player_data = std::make_unique<PlayerData>();
            player_data->username = player->username;
            player_data->texture_path = player->texture_path;
            player_data->x = player->x;
            player_data->y = player->y;
            player_data->width = player->width;
            player_data->height = player->height;
            player_data->tex_x = player->tex_x;
            player_data->tex_y = player->tex_y;
            player_data->tex_width = player->tex_width;
            player_data->tex_height = player->tex_height;
            player_data->going_x = player->x;
            this->state.pushPlayerData(std::move(player_data));
        }

        if (wmessage->message->getTypeID() == MoveMessage::typeID) {
            MoveMessage* move_message = (MoveMessage*)wmessage->message.get();
            v->at(i)->x = move_message->getX();
            v->at(i)->y = move_message->getY();
            v->at(i)->going_x = move_message->getGoingX();
        }
    }

    // remove disconnected players TODO
    std::vector<std::unique_ptr<PlayerData> >* v = state.getPlayersData();
    for (int32_t i = 0; i < v->size(); i++) {
        if ( ! this->auth.isLogged(v->at(i)->username)) {
            int32_t last_id = v->size() - 1;
            std::swap(v->at(i), v->at(last_id));
            v->resize(last_id);
        }
    }

    for (int32_t id : socket_ids) {

        if ( ! this->auth.isLogged(id)) {
            continue;
        }

        server->sendMessage(id, &state);
    }
}

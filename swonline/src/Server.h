#ifndef _SERVER_H_
#define _SERVER_H_
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <vector>
#include <assert.h>
#include <iostream>
#include "../../mwonline/MessageCreator.h"

class Server {
public:
    Server();
    ~Server();

    void init(int32_t port);

    const int32_t getServerId();

    static std::unique_ptr<Message> receiveMessage(
        int32_t socket_id,
        std::vector<uint8_t>& buffer
    );
    static void sendMessage(
        int32_t socket_id,
        Message* message
    );

private:
    bool initialized = false;
    int32_t port;
    int32_t socket_id;

    int32_t server_fd;
    struct sockaddr_in address;
    int32_t addrlen;
};

#endif

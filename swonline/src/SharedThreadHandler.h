#ifndef _SHARED_THREAD_HANDLER_H_
#define _SHARED_THREAD_HANDLER_H_

#include <mutex>
#include <vector>
#include <memory>
#include "Server.h"
#include "WrapMessage.h"

class SharedThreadHandler {
public:
    std::mutex mutex_socket_shared;
    std::unique_ptr<Server> server = nullptr;
    void pushSocketID(int32_t id) {
        mutex_socket_shared.lock();

        this->socket_ids.push_back(id);

        mutex_socket_shared.unlock();
    }
    void getSocketIDs(std::vector<int32_t>* out) {
        mutex_socket_shared.lock();

       *out = this->socket_ids;
        
        mutex_socket_shared.unlock();
    }

    void disconnected(int32_t socket_id) {
        mutex_socket_shared.lock();
        int32_t size = (int32_t) this->socket_ids.size();
        for (int32_t i = 0; i < size; i++) {
            if (this->socket_ids[ i ] == socket_id) {
                this->socket_ids[ i ] = this->socket_ids[ size - 1 ];
                this->socket_ids.resize(size - 1);
            }
        }
        mutex_socket_shared.unlock();
    }

    std::mutex mutex_messages_shared;
    void pushMessages(std::unique_ptr<WrapMessage> message) {
        mutex_messages_shared.lock();

        this->messages.push_back(std::move(message));

        mutex_messages_shared.unlock();
    }

    void getMessages(
        std::vector<std::unique_ptr<WrapMessage>>* out) {
        mutex_messages_shared.lock();

        for (int32_t i = 0; i < this->messages.size(); i++) {
            out->push_back(std::move(this->messages[ i ]));
        }
        this->messages.clear();

        mutex_messages_shared.unlock();
    }

private:
    std::vector<int32_t> socket_ids;
    std::vector<std::unique_ptr<WrapMessage> > messages;
};

class SharedThreadReceiving {
public:
    SharedThreadHandler* shared_thread_handler = nullptr;
    int32_t socket_id;
};

#endif
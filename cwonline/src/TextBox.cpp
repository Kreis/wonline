#include "TextBox.h"

TextBox::TextBox() {
}

void TextBox::init() {
    uint32_t width = Global::getSingleton()->screen_size.x;
    htext_box->init(
        Global::getSingleton()->hfont_config,
        {0, 0, width, 0}
    );
}

void TextBox::draw(HSpriteBatch& hsprite_batch) {
    htext_box->draw(hsprite_batch);
}

void TextBox::clear() {
    this->text = L"";
    this->update();
}

void TextBox::update() {
    HInputState* io = HInputState::getSingleton();
    wchar_t te = static_cast<wchar_t>(io->getTextEntered());
    if (te && htext_box->isValid(te)) {
        this->text += te;
    }
    if (te == '\b') {
        this->text = text.substr(0, text.length() - 1);
    }

    int32_t BLACK_COLOR = 255;
    htext_box->setText(this->text, BLACK_COLOR);

    iVec screen_size = Global::getSingleton()->screen_size;
    fVec view_position = Global::getSingleton()->view_position;

    htext_box->setBox({
        view_position.x - (screen_size.x / 2),
        view_position.y + (screen_size.y / 2),
        screen_size.x,
        0
    });
}

std::wstring TextBox::getText() const {
    return this->text;
}

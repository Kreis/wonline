#include "Map.h"

Map::Map() {

}

void Map::load(std::string path_map, std::string name_file, iVec screen_size) {
    this-> screen_size = screen_size;
    XMLDocument doc;

    doc.LoadFile((path_map + name_file).c_str());

    XMLElement* e_map = doc.FirstChildElement("map");
    uint32_t tilewidth = e_map->IntAttribute("tilewidth");
    uint32_t tileheight = e_map->IntAttribute("tileheight");

    // Iterate tileset
    XMLElement* e_tileset = e_map->FirstChildElement("tileset");
    while (e_tileset != nullptr) {
        std::string tileset_source = e_tileset->Attribute("source");

        // Tileset
        XMLDocument tileset_doc;
        std::string tileset_path = path_map + tileset_source;
        tileset_doc.LoadFile(tileset_path.c_str());
        XMLElement* et_tileset = tileset_doc.FirstChildElement("tileset");

        // Keep tileset!
        MapTileset map_tileset;
        map_tileset.first_gid = e_tileset->IntAttribute("firstgid");
        map_tileset.columns = et_tileset->IntAttribute("columns");
        XMLElement* et_image = et_tileset->FirstChildElement("image");
        std::string image_source = et_image->Attribute("source");
        map_tileset.image_source = std::string(
            image_source.begin() + 3,
            image_source.end()
        );

        map_tilesets.push_back(map_tileset);

        e_tileset = e_tileset->NextSiblingElement("tileset");
    }

    XMLElement* e_layer = e_map->FirstChildElement("layer");
    uint32_t layer_columns = e_layer->IntAttribute("width");
    XMLElement* e_data = e_layer->FirstChildElement("data");

    std::string csv = e_data->GetText();
    std::stringstream ss(csv);
    std::string token;
    while (std::getline(ss, token, ',')) {
        csv_data.push_back(atoi(token.c_str()));
    }

    for (uint32_t i = 0; i < csv_data.size(); i++) {
        uint32_t id = csv_data[ i ];
        MapTileset* map_tileset = nullptr;
        for (MapTileset& m : map_tilesets) {
            if (m.first_gid > id) {
                break;
            }
            map_tileset = &m;
        }

        uint32_t x_coord = i % layer_columns;
        uint32_t y_coord = i / layer_columns;
        uint32_t id_x = (id - 1) % map_tileset->columns;
        uint32_t id_y = (id - 1) / map_tileset->columns;

        hsprites.push_back(HSprite());
        hsprites[ hsprites.size() - 1 ].init(
                map_tileset->image_source,
                {
                    static_cast<float>(id_x * tilewidth),
                    static_cast<float>(id_y * tileheight)
                },
                {
                    static_cast<float>(tilewidth),
                    static_cast<float>(tileheight)
                },
                {
                    static_cast<float>(x_coord * tilewidth),
                    static_cast<float>(y_coord * tileheight)
                },
                {
                    static_cast<float>(tilewidth),
                    static_cast<float>(tileheight)
                }
        );
    }

    // entities
    XMLElement* e_objectgroup = e_map->FirstChildElement("objectgroup");
    while (e_objectgroup != nullptr) {
        std::string eo_name = e_objectgroup->Attribute("name");

        XMLElement* e_object = e_objectgroup->FirstChildElement("object");
        while (e_object != nullptr) {

            entities.push_back(MapEntity());
            MapEntity& map_entity = entities[entities.size() - 1];

            if (eo_name == "targets") {
                map_entity.name = e_object->Attribute("name");
                map_entity.x = e_object->IntAttribute("x");
                map_entity.y = e_object->IntAttribute("y");

            } else if (eo_name == "physics") {
                map_entity.name = eo_name;
                map_entity.x = e_object->IntAttribute("x");
                map_entity.y = e_object->IntAttribute("y");
                map_entity.width = e_object->IntAttribute("width");
                map_entity.height = e_object->IntAttribute("height");
            }

            e_object = e_object->NextSiblingElement("object");
        }

        e_objectgroup = e_objectgroup->NextSiblingElement("objectgroup");
    }
}

void Map::draw(HSpriteBatch& hsprite_batch, fVec screen_position) {
    for (const HSprite& hsprite : hsprites) {
        fVec hpos = hsprite.getPosition();
        fVec hsize = hsprite.getSize();
        if (
            hpos.x >= screen_position.x + (screen_size.x / 2)
            || hpos.y >= screen_position.y + (screen_size.y / 2)
            || hpos.x + hsize.x <= screen_position.x - (screen_size.x / 2)
            || hpos.y + hsize.y <= screen_position.y - (screen_size.x / 2)
        ) {
            continue;
        }

        hsprite_batch.draw(&hsprite);
    }
}

std::vector<MapEntity> Map::getEntities() {
    return entities;
}
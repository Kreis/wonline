#include "Player.h"

Player::Player() {}

void Player::init(std::wstring username, std::string path, fVec tex_pos, fVec tex_size) {
    hsprite.init(path, tex_pos, tex_size, {0, 0}, tex_size);
    hbody = HPhysics::getSingleton()->generateHBody(
        0.0,
        0.0,
        tex_size.x,
        tex_size.y,
        true
    );
    this->username = username;
}

void Player::setPosition(fVec position) {
    HBody* hb = HPhysics::getSingleton()->getHBody(hbody);
    hb->setPosition(position.x, position.y);
}

void Player::moveTo(fVec position) {
    HBody* hb = HPhysics::getSingleton()->getHBody(hbody);
    hb->moveTo(position.x, position.y);
}

void Player::draw(HSpriteBatch& hsprite_batch) {
    HBody* hb = HPhysics::getSingleton()->getHBody(hbody);
    hsprite.setPosition(hb->getX().get(), hb->getY().get());
    hsprite_batch.draw(&hsprite);
}

void Player::move(fVec dir, float delta) {
    HBody* hb = HPhysics::getSingleton()->getHBody(hbody);
    this->last_step_x = dir.x * speed * delta;
    hb->move(last_step_x, dir.y * speed * delta);
    if (dir.x != 0) {
        hsprite.flipHorizontal(dir.x > 0);
    }
}

void Player::fall(float delta) {
    HBody* hb = HPhysics::getSingleton()->getHBody(hbody);
    hb->move(0.0, this->fall_speed * delta);
}

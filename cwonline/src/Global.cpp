#include "Global.h"

Global* Global::singleton = nullptr;

Global* Global::getSingleton() {
    if (singleton == nullptr) {
        singleton = new Global();
    }

    return singleton;
}

Global::~Global() {
    delete singleton;
}

Global::Global() {
    modified_screen_ratio = 1.0f;
}
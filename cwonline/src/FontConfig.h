#ifndef FONT_CONFIG_H_
#define FONT_CONFIG_H_

#include <Herzo/HGui/HFontConfig.h>
#include "tinyxml2.h"
#include <assert.h>

using namespace tinyxml2;

class FontConfig {
public:
    static void generateHFontConfig(
        HFontConfig& hfont_config,
        std::string path
    );
private:
    FontConfig();
};

#endif

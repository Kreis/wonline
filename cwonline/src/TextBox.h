#ifndef _TEXT_BOX_H_
#define _TEXT_BOX_H_

#include <Herzo/HGui/HTextBox.h>
#include <Herzo/HRender/HSpriteBatch.h>
#include <Herzo/HIO/HInputState.h>
#include "Global.h"
#include "tinyxml2.h"
#include <assert.h>

using namespace tinyxml2;

class TextBox {
public:
    TextBox();
    void init();

    void draw(HSpriteBatch& hsprite_batch);
    void update();
    void clear();
    std::wstring getText() const;

private:
    std::wstring text;
    std::unique_ptr<HTextBox> htext_box = std::make_unique<HTextBox>();
    HFontConfig config;
};

#endif
#include <iostream>
#include "Herzo/HNetwork/HBridge.h"
#include "Herzo/HRender/HWindow.h"
#include "Herzo/HIO/HInputState.h"
#include "Herzo/HTime/HTime.h"
#include "Global.h"
#include "FontConfig.h"
#include "Game.h"
#include "../../mwonline/LoginMessage.h"

int main(int argc, char** argv) {
	// // Window
	const uint32_t ORIGINAL_SCREEN_WIDTH = 1440;
	const uint32_t ORIGINAL_SCREEN_HEIGHT = 810;
	HWindow* window = HWindow::getSingleton();
	window->init(ORIGINAL_SCREEN_WIDTH, ORIGINAL_SCREEN_HEIGHT, "Wonline");
	Global::getSingleton()->screen_size = window->getScreenSize();
	Global::getSingleton()->modified_screen_size = window->getScreenSize();

	sf::RenderWindow* render_window = window->render_window;
	render_window->setVerticalSyncEnabled(true);

	// Texture priorities
	HSpriteBatch hsprite_batch;
	hsprite_batch.init({
		"textures/star32-32x.png",
		"textures/colors.png",
		"textures/ph-player-128.png",
		"textures/9patch_60x60.png",
		"textures/9patch_12x12.png",
		"textures/arial50.png"
	});

	// Generate font for all uses (TextBlob and TextBox)
	FontConfig::generateHFontConfig(
		Global::getSingleton()->hfont_config,
		"fonts/arial/50.fnt"
	);
	
	Global* g = Global::getSingleton();

	// Network initializacion
    std::unique_ptr<HBridge> hbridge = std::make_unique<HBridge>("127.0.0.1", 8080);
	hbridge->run();
	g->send_message = [&hbridge](Message* message) {
		std::vector<uint8_t> m = message->getBytes();
		hbridge->send(m);
	};

	// Game intialization
	Game game;
	game.load();
	game.putCallbackMoveView([&window, &g](const iVec& position) {
		window->moveViewTo(position.x, position.y);
		g->view_position = window->getViewPosition();
	});

	HInputState* hInputState = HInputState::getSingleton();

	// Time and frame rate
	HTime htime;
	htime.showFPS = true;
	while ( ! hInputState->closed) {

		// Input load
		hInputState->updateState(render_window);
		if (hInputState->isPressed(sf::Keyboard::Key::LControl)
			&& hInputState->isTyped(sf::Keyboard::Key::W)) {
			break;
		}

		// Update ratio
		if (g->modified_screen_size != window->getScreenSize()) {
			g->modified_screen_ratio = 
				static_cast<float>(ORIGINAL_SCREEN_WIDTH) /
				static_cast<float>(window->getScreenSize().x);
			g->modified_screen_size = window->getScreenSize();
		}

		// Update Net
		std::vector<uint8_t> data = hbridge->poll();
		std::unique_ptr<Message> sm;
		g->state_message = nullptr;
		if (data.size() > 0) {
			sm = MessageCreator::createMessage(data);
			assert(sm->getTypeID() == "STATE");
			g->state_message = (StateMessage*)sm.get();	
		}

		// Update game
		float fixedDelta = htime.fixedElapsedTime();
		float delta = htime.elapsedTime();
		delta = fixedDelta;
		game.update(delta);

		// Render
		hsprite_batch.clear(render_window, sf::Color::Cyan);
		game.draw(hsprite_batch);
		hsprite_batch.flush(render_window);
  	}

	window->close();

	return 0;
}
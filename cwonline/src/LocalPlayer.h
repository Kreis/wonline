#ifndef _LOCAL_PLAYER_H_
#define _LOCAL_PLAYER_H_

#include <memory>
#include <Herzo/HIO/HInputState.h>
#include "../../mwonline/MoveMessage.h"
#include "Player.h"
#include "Global.h"
#include "TextBlob.h"
#include "TextBox.h"

class LocalPlayer {
public:
    void init(std::unique_ptr<Player> player);
    void update(float delta);
    void draw(HSpriteBatch& hsprite_batch);

    std::unique_ptr<Player> player = nullptr;
private:
    void updateBlob(float delta);
    void updateMoving(float delta);
    void updateTextBox(float delta);

    int32_t going_x;
    TextBlob text_blob;
    TextBox text_box;
};

#endif
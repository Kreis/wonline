#include "TextBlob.h"

TextBlob::TextBlob() {
    HTextArea htext_area;
    htext_area.init(
        Global::getSingleton()->hfont_config,
        {
            0, 0,
            600, 600
        }
    );
    vec_htext_area.push_back(htext_area);
    vec_htext_area.push_back(htext_area);
    vec_htext_area.push_back(htext_area);
}

void TextBlob::addText(std::wstring text) {
    if (this->blobs.size() == 3) {
        this->blobs.pop_back();
        this->current_time_blobs.pop_back();
    }
    this->blobs.push_front(text);
    this->current_time_blobs.push_front(this->time_to_read);

    uint32_t htext_area_id = 0;
    for (std::wstring i : this->blobs) {
        uint32_t BLACK = 255;
        HTextArea& htext_area = vec_htext_area[ htext_area_id++ ];
        htext_area.setText(i, BLACK, HTextAreaAlign::MIDDLE);
    }
    this->setPosition(this->position);
}

void TextBlob::update(float delta) {
    uint32_t htext_area_id = 0;
    for (float& current_time : this->current_time_blobs) {
        HTextArea& htext_area = vec_htext_area[ htext_area_id++ ];
        current_time -= delta;
        if (current_time < 0.0f) {
            htext_area.clear();
            current_time = 0.0f;
        }
    }

    while (
        this->current_time_blobs.size() > 0 &&
        this->current_time_blobs.back() == 0.0f
    ) {
        this->blobs.pop_back();
        this->current_time_blobs.pop_back();
    }
    
}

void TextBlob::setPosition(const iVec& position) {
    this->position = position;

    uint32_t htext_area_id = 0;
    uint32_t new_space_occupied = this->text_y_offset;
    for (std::wstring i : this->blobs) {
        HTextArea& htext_area = this->vec_htext_area[ htext_area_id++ ];
        const iRect& bounds = htext_area.getBounds();
        htext_area.setPosition({
            this->position.x - bounds.w / 2,
            this->position.y - bounds.h - new_space_occupied
        });
        new_space_occupied += bounds.h + this->text_y_offset;
    }
}

void TextBlob::draw(HSpriteBatch& hsprite_batch) {
    uint32_t htext_area_id = 0;
    for (std::wstring i : this->blobs) {
        HTextArea& htext_area = this->vec_htext_area[ htext_area_id++ ];
        htext_area.draw(hsprite_batch);
    }
}

#include "LocalPlayer.h"

void LocalPlayer::init(std::unique_ptr<Player> player) {
    this->player = std::move(player);
    HBody* hbody = HPhysics::getSingleton()->getHBody(this->player->hbody);
    this->going_x = hbody->getX().get();

	this->text_box.init();
}

void LocalPlayer::update(float delta) {
    this->updateTextBox(delta);
    this->updateBlob(delta);
    this->updateMoving(delta);
}

void LocalPlayer::updateMoving(float delta) {

    if (HInputState::getSingleton()->isTyped(sf::Mouse::Right)) {
        // get from mouse at screen
        iVec pos = HInputState::getSingleton()->getMousePosition();
        Global* g = Global::getSingleton();

        // update ratio in case of screen resize
        this->going_x = pos.x * g->modified_screen_ratio;

        // fix with view position
        this->going_x = going_x + g->view_position.x - g->screen_size.x / 2;

        MoveMessage m;
        m.setGoingX(this->going_x);
        m.setX(pos.x);
        m.setY(pos.y);
        Global::getSingleton()->send_message((Message*)&m);
    }
    // Moving with click
    HBody* hbody = HPhysics::getSingleton()->getHBody(this->player->hbody);
    float current_x = hbody->getX().get() + hbody->getWidth().get() / 2.0f;
    if (current_x != this->going_x) {

        bool in_left = current_x < this->going_x;
        if (in_left) {
            this->player->move({1, 0}, delta);
        }
        bool in_right = current_x > this->going_x;
        if (in_right) {
            this->player->move({-1, 0}, delta);
        }
        float last_step_x = this->player->last_step_x;

        // this means that pj pass the destination (bucle -> <-)
        if (
                (in_left && current_x + last_step_x > this->going_x)
            ||  (in_right && current_x + last_step_x < this->going_x)
         ) {
            this->player->moveTo({
                static_cast<float>(this->going_x) - hbody->getWidth().get() / 2.0f,
                hbody->getY().get()
            });
        }
    }

    this->player->fall(delta);
}

void LocalPlayer::updateBlob(float delta) {
    HBody* hbody = HPhysics::getSingleton()->getHBody(this->player->hbody);
    this->text_blob.setPosition(
        {
            hbody->getX() + hbody->getWidth() / 2,
            hbody->getY() - 32
        }
    );
    this->text_blob.update(delta);
}

void LocalPlayer::updateTextBox(float delta) {
    if (HInputState::getSingleton()->isTyped(sf::Keyboard::Enter)) {
        this->text_blob.addText(this->text_box.getText());
        this->text_box.clear();
    }
    this->text_box.update();
}

void LocalPlayer::draw(HSpriteBatch& hsprite_batch) {
    this->player->draw(hsprite_batch);
    this->text_blob.draw(hsprite_batch);
    this->text_box.draw(hsprite_batch);
}

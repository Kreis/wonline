#include "NinePatch.h"

NinePatch::NinePatch() {

}
NinePatch::~NinePatch() {

}

void NinePatch::init(
    std::string texture_path,
    iRect texture_rect,
    iRect bounds
) {
    // works for square patchs
    int32_t third_rect = texture_rect.w / 3;
    int32_t middle_wid = bounds.w - (third_rect * 2);
    int32_t middle_hei = bounds.h - (third_rect * 2);
    std::array<iRect, 9> rects = {
        iRect(
            bounds.x, bounds.y,
            third_rect, third_rect
        ),
        iRect(
            bounds.x + third_rect, bounds.y,
            middle_wid, third_rect
        ),
        iRect(
            bounds.x + third_rect + middle_wid, bounds.y,
            third_rect, third_rect
        ),
        iRect(
            bounds.x, bounds.y + third_rect,
            third_rect, middle_hei
        ),
        iRect(
            bounds.x + third_rect, bounds.y + third_rect,
            middle_wid, middle_hei
        ),
        iRect(
            bounds.x + third_rect + middle_wid, bounds.y + third_rect,
            third_rect, middle_hei
        ),
        iRect(
            bounds.x, bounds.y + third_rect + middle_hei,
            third_rect, third_rect
        ),
        iRect(
            bounds.x + third_rect, bounds.y + third_rect + middle_hei,
            middle_wid, third_rect
        ),
        iRect(
            bounds.x + third_rect + middle_wid, bounds.y + third_rect + middle_hei,
            third_rect, third_rect
        )
    };


    for (int32_t i = 0; i < 3; i++) {
        for (int32_t j = 0; j < 3; j++) {
            this->hsprites[ i * 3 + j ].init(
                texture_path,
                { 
                    texture_rect.x + (j * third_rect),
                    texture_rect.y + (i * third_rect)
                },
                {
                    third_rect,
                    third_rect
                },
                {
                    rects[ i * 3 + j ].x,
                    rects[ i * 3 + j ].y
                },
                {
                    rects[ i * 3 + j ].w,
                    rects[ i * 3 + j ].h
                }
            );
        }
    }

    this->initialized = true;
}

void NinePatch::adjust(iRect bounds) {
    assert(this->initialized);
    int32_t third = bounds.w / 3;
    for (int32_t i = 0; i < 3; i++) {
        for (int32_t j = 0; j < 3; j++) {
            this->hsprites[ i * 3 + j ].setPosition(
                bounds.x + (j * (bounds.w / 3)),
                bounds.y + (i * (bounds.h / 3))
            );
        }
    }
}

void NinePatch::draw(HSpriteBatch& hsprite_batch) {
    assert(this->initialized);
    for (HSprite& hsprite : this->hsprites) {
        hsprite_batch.draw(&hsprite);
    }
}

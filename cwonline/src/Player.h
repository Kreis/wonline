#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <iostream>
#include <Herzo/HRender/HSprite.h>
#include <Herzo/HRender/HSpriteBatch.h>
#include <Herzo/HPhysics/HPhysics.h>

class Player {
public:
    Player();
    void init(std::wstring username, std::string path, fVec tex_pos, fVec tex_size);
    void setPosition(fVec position);
    void moveTo(fVec position);
    void draw(HSpriteBatch& hsprite_batch);
    void update(float delta);
    void move(fVec dir, float delta);
    void fall(float delta);

    uint32_t hbody;
    float last_step_x = 0.0f;
    std::wstring username;

private:
    HSprite hsprite;

    float speed = 200;
    float fall_speed = 1100;
};

#endif
#include "RemotePlayer.h"

void RemotePlayer::init(std::unique_ptr<Player> player) {
    this->player = std::move(player);
    HBody* hbody = HPhysics::getSingleton()->getHBody(this->player->hbody);
    this->going_x = hbody->getX().get();
}

void RemotePlayer::update(float delta) {
    StateMessage* sm = Global::getSingleton()->state_message;
    if (sm != nullptr) {
        int32_t i;
        std::vector<std::unique_ptr<PlayerData> >* v = sm->getPlayersData();
        for (i = 0; i < v->size(); i++) {
            if (v->at(i)->username == this->player->username) {

                this->going_x = v->at(i)->going_x;

                break;
            }
        }

    }

    HBody* hbody = HPhysics::getSingleton()->getHBody(this->player->hbody);
    float current_x = hbody->getX().get() + hbody->getWidth().get() / 2.0f;

        if (current_x != this->going_x) {

        bool in_left = current_x < this->going_x;
        if (in_left) {
            this->player->move({1, 0}, delta);
        }
        bool in_right = current_x > this->going_x;
        if (in_right) {
            this->player->move({-1, 0}, delta);
        }
        float last_step_x = this->player->last_step_x;

        // this means that pj pass the destination (bucle -> <-)
        if (
                (in_left && current_x + last_step_x > this->going_x)
            ||  (in_right && current_x + last_step_x < this->going_x)
         ) {
            this->player->moveTo({
                static_cast<float>(this->going_x) - hbody->getWidth().get() / 2.0f,
                hbody->getY().get()
            });
        }
    }
}

void RemotePlayer::draw(HSpriteBatch& hsprite_batch) {
    this->player->draw(hsprite_batch);

    
}

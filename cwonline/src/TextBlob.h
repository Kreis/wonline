#ifndef TEXT_BLOB_H_
#define TEXT_BLOB_H_

#include <Herzo/HRender/HSpriteBatch.h>
#include <Herzo/HGui/HFontConfig.h>
#include <Herzo/HGui/HTextArea.h>
#include <list>
#include "Global.h"

class TextBlob {
public:
    TextBlob();

    void addText(std::wstring text);
    void update(float delta);
    void setPosition(const iVec& position);
    void draw(HSpriteBatch& hsprite_batch);

private:
    std::vector<HTextArea> vec_htext_area;
    std::list<std::wstring> blobs;
    std::list<float> current_time_blobs;
    float time_to_read = 7.0;
    uint32_t width = 300;
    uint32_t text_y_offset = 16;
    iVec position;
};

#endif

#ifndef _LOGIN_H_
#define _LOGIN_H_

#include <iostream>
#include <functional>
#include <Herzo/HIO/HInputState.h>
#include <Herzo/HGui/HTextBox.h>
#include <Herzo/HRender/HSpriteBatch.h>
#include "Global.h"
#include "NinePatch.h"
#include "GameStatus.h"

class Login {
public:
    Login();
    ~Login();

    void init(std::function<void(std::wstring username)> callbackStatusToName);
    void update(float delta);
    void draw(HSpriteBatch& hsprite_batch);

private:
    bool initialized = false;
    std::wstring text;
    std::unique_ptr<HTextBox> htextbox = std::make_unique<HTextBox>();
    iRect textbox_bounds;
    NinePatch nine_path;
    std::function<void(std::wstring username)> callbackStatusToName = nullptr;
};

#endif

#include "FontConfig.h"

FontConfig::FontConfig() {

}

void FontConfig::generateHFontConfig(
    HFontConfig& hfont_config,
    std::string path
) {
    XMLDocument doc;

    doc.LoadFile(path.c_str());

    XMLElement* e_font = doc.FirstChildElement("font");
    XMLElement* e_info = e_font->FirstChildElement("common");
    uint32_t lineHeight = e_info->IntAttribute("lineHeight");

    std::map<std::string, std::string> map_id_texture_name;
    XMLElement* e_pages = e_font->FirstChildElement("pages");
    XMLElement* e_page = e_pages->FirstChildElement("page");
    while (e_page != nullptr) {
        std::string e_page_id = e_page->Attribute("id");
        std::string e_page_file = e_page->Attribute("file");

        map_id_texture_name[e_page_id] = e_page_file;
        e_page = e_page->NextSiblingElement("page");
    }

    std::vector<uint8_t> vec_ascii;
    std::vector<iRect> vec_texture_area;
    std::vector<iVec> vec_offset_position;
    std::vector<uint32_t> vec_xadvance;
    std::vector<std::string> vec_textures_name;
    XMLElement* e_chars = e_font->FirstChildElement("chars");
    XMLElement* e_char = e_chars->FirstChildElement("char");
    while (e_char != nullptr) {
        char ascii_letter = e_char->IntAttribute("id");
        uint32_t x = e_char->IntAttribute("x");
        uint32_t y = e_char->IntAttribute("y");
        uint32_t width = e_char->IntAttribute("width");
        uint32_t height = e_char->IntAttribute("height");
        uint32_t xoffset = e_char->IntAttribute("xoffset");
        uint32_t yoffset = e_char->IntAttribute("yoffset");
        uint32_t xadvance = e_char->IntAttribute("xadvance");
        std::string page = e_char->Attribute("page");
        std::string texture_name = map_id_texture_name.at(page);

        vec_ascii.push_back(ascii_letter);
        vec_texture_area.push_back({x, y, width, height});
        vec_offset_position.push_back({xoffset, yoffset});
        vec_xadvance.push_back(xadvance);
        vec_textures_name.push_back(texture_name);

        e_char = e_char->NextSiblingElement("char");
    }

    std::vector<uint8_t> firsts;
    std::vector<uint8_t> seconds;
    std::vector<int8_t> amounts;
    XMLElement* e_kernings = e_font->FirstChildElement("kernings");
    XMLElement* e_kerning = e_kernings->FirstChildElement("kerning");
    while (e_kerning != nullptr) {
        char first = e_kerning->IntAttribute("first");
        char second = e_kerning->IntAttribute("second");
        int8_t amount = e_kerning->IntAttribute("amount");
        
        firsts.push_back(first);
        seconds.push_back(second);
        amounts.push_back(amount);

        e_kerning = e_kerning->NextSiblingElement("kerning");
    }

    hfont_config.setLineHeight(lineHeight);
    hfont_config.setSpaceWidth(9);
    hfont_config.setLetters(
        vec_ascii,
        vec_texture_area,
        vec_offset_position,
        vec_xadvance,
        vec_textures_name
    );
    hfont_config.setKernings(
        firsts,
        seconds,
        amounts
    );

    hfont_config.validate();
}

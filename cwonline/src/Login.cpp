#include "Login.h"

Login::Login() {

}

Login::~Login() {

}

void Login::init(std::function<void(std::wstring username)> callbackStatusToName) {
    this->callbackStatusToName = callbackStatusToName;
    iVec size = Global::getSingleton()->screen_size;
    textbox_bounds = {
        size.x / 2 - 80,
        size.y / 2 - 60,
        160,
        Global::getSingleton()->hfont_config.getLineHeight()
    };
    this->htextbox->init(
        Global::getSingleton()->hfont_config,
        textbox_bounds
    );

    iVec offset = { 40, 40 };
    this->nine_path.init(
        "textures/9patch_12x12.png",
        {0, 0, 12, 12},
        {
            textbox_bounds.x - offset.x / 2,
            textbox_bounds.y - textbox_bounds.h - offset.x / 2,
            textbox_bounds.w + offset.x,
            textbox_bounds.h + offset.y
        }
    );
    this->initialized = true;
}

void Login::update(float delta) {
    assert(this->initialized);
    HInputState* io = HInputState::getSingleton();
    wchar_t te = static_cast<wchar_t>(io->getTextEntered());
    if (HInputState::getSingleton()->isTyped(sf::Keyboard::Enter)) {
        this->callbackStatusToName(this->text);
    }
    if (te && htextbox->isValid(te)) {
        this->text += te;
    }
    if (te == '\b') {
        this->text = text.substr(0, text.length() - 1);
    }

    uint32_t BLACK_COLOR = 255;
    htextbox->setText(this->text, BLACK_COLOR);
}

void Login::draw(HSpriteBatch& hsprite_batch) {
    assert(this->initialized);
    nine_path.draw(hsprite_batch);
    htextbox->draw(hsprite_batch);
}

#ifndef _REMOTE_PLAYER_H_
#define _REMOTE_PLAYER_H_

#include <iostream>
#include "Global.h"
#include "Player.h"
#include <memory>

class RemotePlayer {
public:
    void init(std::unique_ptr<Player> player);
    void update(float delta);
    void draw(HSpriteBatch& hsprite_batch);

    std::unique_ptr<Player> player = nullptr;

private:
    int32_t going_x;
};

#endif

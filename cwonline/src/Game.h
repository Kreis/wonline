#include <iostream>
#include <Herzo/HRender/HSpriteBatch.h>
#include <Herzo/HNetwork/HBridge.h>
#include "../../mwonline/MessageCreator.h"
#include "Map.h"
#include "LocalPlayer.h"
#include "RemotePlayer.h"
#include "Global.h"
#include "Login.h"
#include "GameStatus.h"

class Game {
public:
    Game();

    void load();

    void putCallbackMoveView(
        std::function<void(const iVec& position)> callback
    ) {
        this->callbackMoveView = callback;
    }
    void update(float delta);
    void draw(HSpriteBatch& hsprite_batch);

private:
    GameStatus current_status = GameStatus::LOGIN;
	Map map;
	LocalPlayer local_player;
    std::vector<RemotePlayer> remote_players;
    Login login;
    std::function<void(const iVec& position)> callbackMoveView = nullptr;
    void statusToGame(std::wstring username);

    void loadPlayer(PlayerData* player_data);
    void updateRunning(float delta);
    void updateWaitingResponse(float delta);
    void updateLogin(float delta);
    void updateRemotePlayers(float delta);

    void drawRunning(HSpriteBatch& hsprite_batch);
    void drawLogin(HSpriteBatch& hsprite_batch);
    void drawRemotePlayers(HSpriteBatch& hsprite_batch);

    std::wstring username;
};

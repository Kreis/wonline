#ifndef _GAME_STATUS_H_
#define _GAME_STATUS_H_

enum class GameStatus {
    LOGIN,
    WAITING_RESPONSE,
    RUNNING
};

#endif

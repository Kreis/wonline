#include "Game.h"

Game::Game() {

}

void Game::loadPlayer(PlayerData* player_data) {
	std::unique_ptr<Player> _player = std::make_unique<Player>();
	_player->init(
		player_data->username,
		player_data->texture_path,
		{ player_data->tex_x, player_data->tex_y },
		{ player_data->tex_width, player_data->tex_height }
	);
	_player->setPosition({
		player_data->x,
		player_data->y
	});

	this->local_player.init(std::move(_player));
}

void Game::load() {

	login.init(
		std::bind(
			&Game::statusToGame,
			this,
			std::placeholders::_1
		)
	);

	// Map
	map.load("map/", "place.tmx", Global::getSingleton()->screen_size);
	std::vector<MapEntity> map_entities = map.getEntities();
	for (const MapEntity& m : map_entities) {
		if (m.name == "physics") {
			HPhysics::getSingleton()->generateHBody(
				m.x, m.y, m.width, m.height
			);
		}
	}


}

void Game::update(float delta) {
	if (this->current_status == GameStatus::LOGIN) {
		this->updateLogin(delta);
	} else if (this->current_status == GameStatus::WAITING_RESPONSE) {
		this->updateWaitingResponse(delta);
	} else if (this->current_status == GameStatus::RUNNING) {
		this->updateRunning(delta);
	} else {
		// unhandled game status
		assert(false);
	}
}

void Game::updateWaitingResponse(float delta) {
	static float waiting_time = 0;
	waiting_time += delta;

	if (waiting_time >= 5.0) {
		std::cout << "turning back to login" << std::endl;
		// time out, return to login
		waiting_time = 0;
		this->current_status = GameStatus::LOGIN;
		return;
	}

	StateMessage* sm = Global::getSingleton()->state_message;
	if (sm == nullptr) {
		return;
	}

	std::vector<std::unique_ptr<PlayerData> >* vpd = sm->getPlayersData();
	for (std::unique_ptr<PlayerData>& pd : *vpd) {
		if (pd->username == this->username) {
			this->loadPlayer(pd.get());
			this->current_status = GameStatus::RUNNING;
			return;
		}
	}
}

void Game::updateRunning(float delta) {
    // Update physics
    HPhysics::getSingleton()->update();

    // Camera following main pj
    uint32_t _id = local_player.player->hbody;
    float pj_x = HPhysics::getSingleton()->getHBody(_id)->getX().get();
    float pj_y = HPhysics::getSingleton()->getHBody(_id)->getY().get();

    int32_t screen_height = Global::getSingleton()->screen_size.y;
    this->callbackMoveView({pj_x, pj_y - screen_height / 4});

	// Update local player with text screen
	// (it will be updated in physics until next frame)
    this->local_player.update(delta);
	this->updateRemotePlayers(delta);
}

void Game::updateLogin(float delta) {
	login.update(delta);
}

void Game::updateRemotePlayers(float delta) {
	Global* g = Global::getSingleton();
	StateMessage* sm = g->state_message;
	std::vector<std::unique_ptr<PlayerData> >* v = nullptr;
	if (sm != nullptr) {
		v = sm->getPlayersData();
	}

	// New Loggins
	if (v != nullptr) for (std::unique_ptr<PlayerData>& p : *v) {
		if (p->username == this->local_player.player->username) {
			continue;
		}

		bool summoned = false;
		for (RemotePlayer& rm : this->remote_players) {
			if (p->username == rm.player->username) {
				summoned = true;
				break;
			}
		}
		if (summoned) {
			continue;
		}

		int32_t id = this->remote_players.size();
		this->remote_players.push_back(RemotePlayer());
		RemotePlayer& rp = this->remote_players[ id ];

		std::unique_ptr<Player> rplayer = std::make_unique<Player>();
		rplayer->init(
			p->username,
			p->texture_path,
			{ p->tex_x, p->tex_y },
			{ p->tex_width, p->tex_height }
		);
		rplayer->setPosition({ p->x, p->y });
		rp.init(std::move(rplayer));
	}

	// Remove disconnected
	if (v != nullptr) for (RemotePlayer& rp : this->remote_players) {
		bool disconnected = true;
		for (std::unique_ptr<PlayerData>& p : *v) {
			if (rp.player->username == p->username) {
				disconnected = false;
				break;
			}
		}

		if (disconnected) {
			int32_t last_id = this->remote_players.size() - 1;
			std::swap(rp, this->remote_players[ last_id ]);
			this->remote_players.resize(last_id);
		}
	}

	for (RemotePlayer& rm : this->remote_players) {
		rm.update(delta);
	}
}

void Game::draw(HSpriteBatch& hsprite_batch) {
	if (this->current_status == GameStatus::LOGIN) {
		this->drawLogin(hsprite_batch);
	} else if (this->current_status == GameStatus::WAITING_RESPONSE) {

	} else if (this->current_status == GameStatus::RUNNING) {
		this->drawRunning(hsprite_batch);
	} else {
		// unhandled game status
		assert(false);
	}
}

void Game::drawRunning(HSpriteBatch& hsprite_batch) {
	fVec view_position = Global::getSingleton()->view_position;
    this->map.draw(hsprite_batch, view_position);

	this->drawRemotePlayers(hsprite_batch);
    this->local_player.draw(hsprite_batch);
}

void Game::drawLogin(HSpriteBatch& hsprite_batch) {
	login.draw(hsprite_batch);
}

void Game::drawRemotePlayers(HSpriteBatch& hsprite_batch) {
	for (RemotePlayer& rm : this->remote_players) {
		rm.draw(hsprite_batch);
	}
}

// called from Login
void Game::statusToGame(std::wstring username) {
	std::wcout << "Login " << username << std::endl;
	this->username = username;

	LoginMessage loginMessage;
	loginMessage.setUsername(username);
	Global::getSingleton()->send_message(&loginMessage);

	this->current_status = GameStatus::WAITING_RESPONSE;
}

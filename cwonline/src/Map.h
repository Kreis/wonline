#ifndef _MAP_H_
#define _MAP_H_
#include <iostream>
#include <sstream>
#include <vector>
#include <Herzo/HRender/HSpriteBatch.h>
#include <Herzo/HRender/HSprite.h>
#include "tinyxml2.h"

class MapTileset {
public:
    uint32_t columns;
    std::string image_source;
    uint32_t first_gid;
};

class MapEntity {
public:
    std::string name;
    int32_t x;
    int32_t y;
    int32_t width;
    int32_t height;
};

using namespace tinyxml2;
class Map {
public:
    Map();
    void load(std::string path_map, std::string name_file, iVec screen_size);
    void draw(HSpriteBatch& hsprite_batch, fVec screen_position);

    std::vector<MapEntity> getEntities();

private:
    iVec screen_size;
    std::vector<uint32_t> csv_data;
    std::vector<HSprite> hsprites;
    std::vector<MapEntity> entities;
    std::vector<MapTileset> map_tilesets;
};

#endif
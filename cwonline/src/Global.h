#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <Herzo/common/HDataType.h>
#include <Herzo/HGui/HFontConfig.h>
#include "../../mwonline/StateMessage.h"
#include <functional>

class Global {
public:
    ~Global();

    static Global* getSingleton();
    fVec view_position;
    iVec screen_size;
    iVec modified_screen_size;
    float modified_screen_ratio;
    HFontConfig hfont_config;
    StateMessage* state_message = nullptr;
    std::function<void(Message*)> send_message = nullptr;

private:
    Global();
    static Global* singleton;
};

#endif
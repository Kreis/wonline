#ifndef _NINE_PATCH_H_
#define _NINE_PATCH_H_

#include <iostream>
#include <Herzo/common/HDataType.h>
#include <Herzo/HRender/HSpriteBatch.h>
#include <Herzo/HRender/HSprite.h>
#include <array>

class NinePatch {
public:
    NinePatch();
    ~NinePatch();

    void init(
        std::string texture_path,
        iRect texture_rect,
        iRect bounds);
    void adjust(iRect bounds);
    void draw(HSpriteBatch& hsprite_batch);

private:
    bool initialized = false;
    std::array<HSprite, 9> hsprites;
};

#endif
#include <iostream>
#include <cstring>
#include "../mwonline/MessageCreator.h"
#include "../cwonline/3rdParty/Herzo/HNetwork/HBridge.h"

// g++ -pthread -std=c++17 *.cpp ../mwonline/*.cpp ../cwonline/3rdParty/Herzo/herzo.a -I$(pwd)/../cwonline/3rdParty -o wtest -Wno-narrowing && ./wtest

int main(int argc, char** argv) {
    std::cout << "start" << std::endl;

	// std::unique_ptr<StateMessage> m = std::make_unique<StateMessage>();

	// std::unique_ptr<EntityData> entity = std::make_unique<EntityData>();
	// entity->name = "Light";
	// entity->status = "Off";
	// std::unique_ptr<EntityData> entity2 = std::make_unique<EntityData>();
	// entity2->name = "Moon";
	// entity2->status = "Up";
	// m->pushEntityData(std::move(entity));
	// m->pushEntityData(std::move(entity2));

	// std::unique_ptr<PlayerData> player1 = std::make_unique<PlayerData>();
	// player1->username = L"yceraf_5454";
	// player1->texture_path = "textures/image.png";
	// player1->x = 20;
	// player1->y = 21;
	// player1->width = 22;
	// player1->height = 23;
	// player1->going_x = 24;
	// player1->tex_x = 25;
	// player1->tex_y = 26;
	// player1->tex_width = 27;
	// player1->tex_height = 28;
	// m->pushPlayerData(std::move(player1));
	
	// std::unique_ptr<PlayerData> player2 = std::make_unique<PlayerData>();
	// player2->username = L"yceraf_0101";
	// player2->texture_path = "textures/image.png";
	// player2->x = 20;
	// player2->y = 21;
	// player2->width = 22;
	// player2->height = 23;
	// player2->going_x = 24;
	// player2->tex_x = 25;
	// player2->tex_y = 26;
	// player2->tex_width = 27;
	// player2->tex_height = 28;
	// m->pushPlayerData(std::move(player2));

	// std::vector<uint8_t> d = m->getBytes();
	// std::cout << "size: " << d.size() << std::endl;

	// StateMessage sm;
	// sm.createFromBytes(d);

	// std::cout << sm.getEntitiesData()->at(0)->name << std::endl;
	// std::cout << sm.getEntitiesData()->at(0)->status << std::endl;
	// std::wcout << sm.getPlayersData()->at(0)->username << std::endl;
	// std::cout << sm.getPlayersData()->at(0)->going_x << std::endl;
	// std::wcout << sm.getPlayersData()->at(1)->username << std::endl;
	// std::cout << sm.getPlayersData()->at(1)->going_x << std::endl;

	// return 0;

	// TESTING CONNECTIONS
	HBridge bridge("127.0.0.1", 8080);
	bridge.run();

	std::string message;
	
	while (true) {
		printf("command\n");
		std::cin >> message;
		
		if (message == "exit") {
			break;
		}

		if (message.substr(0, strlen("one1")) == "one1") {
			LoginMessage message;
			message.setUsername(L"yceraf_5454");
			std::vector<uint8_t> v = message.getBytes();
			bridge.send(v);
		} else if (message.substr(0, strlen("one2")) == "one2") {
			LoginMessage message;
			message.setUsername(L"yceraf_0101");
			std::vector<uint8_t> v = message.getBytes();
			bridge.send(v);
			
		} else if (message.substr(0, strlen("two")) == "two") {

			MoveMessage message;
			message.setX(45);
			message.setY(411);
			message.setGoingX(80);

			std::vector<uint8_t> v = message.getBytes();
			std::cout << message.getY() << std::endl;

			MoveMessage messag2;
			messag2.createFromBytes(v);
			std::cout << messag2.getX() << std::endl;
			std::cout << messag2.getY() << std::endl;
			std::cout << messag2.getGoingX() << std::endl;

			bridge.send(v);

		} else if (message.substr(0, strlen("three")) == "three") {
			std::vector<uint8_t> rmsg;
			while (rmsg.size() == 0) {
				sleep(1);
				std::cout << "polling..." << std::endl;
				rmsg = bridge.poll();

				if ( ! bridge.is_alive()) {
					std::cout << "try to reconnect" << std::endl;
				}
			}
            auto m = MessageCreator::createMessage(rmsg);
            std::cout << "type: " << m->getTypeID() << std::endl;
			if (m->getTypeID() == "STATE") {
				StateMessage* sm = (StateMessage*)m.get();
				std::cout << "state received: " << std::endl;
				std::vector<std::unique_ptr<PlayerData> >* v = sm->getPlayersData();
				std::cout << "PlayersData size " << v->size() << std::endl;
				for (std::unique_ptr<PlayerData>& pd : (*v)) {
					std::wcout << pd->username << std::endl;
					std::cout << pd->texture_path << std::endl;
					std::cout << pd->x << std::endl;
					std::cout << pd->y << std::endl;
					std::cout << pd->width << std::endl;
					std::cout << pd->height << std::endl;
					std::cout << pd->tex_x << std::endl;
					std::cout << pd->tex_y << std::endl;
					std::cout << pd->tex_width << std::endl;
					std::cout << pd->tex_height << std::endl;
					std::cout << pd->going_x << std::endl;
				}
			} else {
				std::cout << "Not state" << std::endl;
			}
            
		}
	}

    return 0;
}
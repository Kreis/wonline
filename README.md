# Wonline

## description

- Indie MMORPG game
- Developed with C++ client side and Elixir-lang server side

## requirement

include 3rdParty SFML libraries, getted from
`git@gitlab.com:Kreis/kibey.git`
and put it in `wonline`

### For windows
include the `.dll` files into the executable

## compile & run

### Unix environment
```sh
# server
cd wonline/swonline
mix run --no-halt

# client
cd wonline/cwonline

mkdir -p 3rdParty
cd 3rdParty
git clone git@gitlab.com:Kreis/herzo.git
cd ..
mkdir -p build
cd build

# copy dependencies to OS
# go to sfml path ->
# sudo cp SFML/unix/lib/*.so* /usr/lib

g++ -std=c++17 ../src/*.cpp ../../mwonline/*.cpp ../3rdParty/Herzo/herzo.a -I$(pwd)/../3rdParty -I$(pwd)/../3rdParty/SFML/unix/include/ -L$(pwd)/../3rdParty/SFML/unix/lib/ -pthread -lsfml-system -lsfml-window -lsfml-audio -lsfml-graphics -lsfml-network -o won

# sometimes used -lws2_32

# without sfml
g++ -std=c++0x src/*.cpp -pthread -o build/won
./build/won
```



# Interface

receiving
-------------
* SEND|SIGNIN|`name`
* SEND|MOVING|`going-to-x-value`
* SEND|STOP|`final-x`|`direction`
* SEND|CHAT|`byte size of message`|`message`
* GET|STATUS|`complete` [Invoke sending status]

> Note: the | symbols are symbolic. remove them (don't send them in a real message)

name | type
-----|------
name | string
going-to-x-value | uint32
final-x | uint32
direction | ENUM: LEFT RIGHT
message | string
complete | boolean: 0 1

> `complete` says to the server that you need the complete server status, otherwise, the server will send just the new changes vs the last status sent it to that socket

sending status
-------------
* `byte size of message`\
    STATUS\
    &name=`name`&x=`x coord`&direction=`direction`&message=`message`\
    &name=`name`&x=`x coord`&direction=`direction`&message=`message`\
    .\
    .\
    .

name | type
-----|------
byte size of message | uint32
name | string
x | uint32
direction | Enum: LEFT RIGHT
message | string


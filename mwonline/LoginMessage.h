#ifndef _LOGIN_MESSAGE_H_
#define _LOGIN_MESSAGE_H_

#include "Message.h"

class LoginMessage : public Message {
public:
    LoginMessage();
    void createFromBytes(const std::vector<uint8_t>& request) override;
    std::vector<uint8_t> getBytes() override;
    std::string getTypeID() override;
    void setUsername(std::wstring username);
    std::wstring getUsername();

    const static std::string typeID;
private:
    std::vector<uint8_t> data;
    std::wstring username;
    bool need_recalculate_bytes = false;
};

#endif
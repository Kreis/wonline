#ifndef _STATE_MESSAGE_H_
#define _STATE_MESSAGE_H_

#include "Message.h"

class PlayerData {
public:
    std::wstring username;
    int32_t x;
    int32_t y;
    int32_t width;
    int32_t height;
    int32_t going_x;
    std::string texture_path;
    
    int32_t tex_x;
    int32_t tex_y;
    int32_t tex_width;
    int32_t tex_height;
};

class EntityData {
public:
    std::string name;
    std::string status;
};

class StateMessage : public Message {
public:
    StateMessage();
    void createFromBytes(const std::vector<uint8_t>& request) override;
    std::vector<uint8_t> getBytes() override;
    std::string getTypeID() override;

    void pushPlayerData(std::unique_ptr<PlayerData> player_data);
    void pushEntityData(std::unique_ptr<EntityData> entity_data);

    std::vector<std::unique_ptr<PlayerData> >* getPlayersData();
    std::vector<std::unique_ptr<EntityData> >* getEntitiesData();

    const static std::string typeID;

private:
    std::vector<uint8_t> data;

    std::vector<std::unique_ptr<PlayerData> > players;
    std::vector<std::unique_ptr<EntityData> > entities;

    bool need_recalculate_bytes = false;
};

#endif

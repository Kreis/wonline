#include "MoveMessage.h"

const std::string MoveMessage::typeID = "MOVE";

MoveMessage::MoveMessage() {

}

void MoveMessage::createFromBytes(const std::vector<uint8_t>& request) {
    int32_t start_id = 4 + this->getTypeID().length();
    BitsHelper::getValue(
        request, start_id, &this->x
    );
    BitsHelper::getValue(
        request, start_id + 4, &this->y
    );
    BitsHelper::getValue(
        request, start_id + 8, &this->going_x
    );

    this->data = request;
    this->need_recalculate_bytes = false;
}

std::vector<uint8_t> MoveMessage::getBytes() {
    if (this->need_recalculate_bytes) {
        this->data.clear();
        BitsHelper::setValue(this->getTypeID().length(), this->data);
        BitsHelper::setValue(this->getTypeID(), this->data);
        BitsHelper::setValue(this->x, this->data);
        BitsHelper::setValue(this->y, this->data);
        BitsHelper::setValue(this->going_x, this->data);

        this->need_recalculate_bytes = false;
    }

    assert(this->data.size() > 0);
    std::cout << this->data.size() << std::endl;
    std::cout << (int32_t)this->data[ 4 + 4 + 4 ] << std::endl;
    std::cout << (int32_t)this->data[ 4 + 4 + 4 + 1 ] << std::endl;
    std::cout << (int32_t)this->data[ 4 + 4 + 4 + 2 ] << std::endl;
    std::cout << (int32_t)this->data[ 4 + 4 + 4 + 3 ] << std::endl;

    return this->data;
}

std::string MoveMessage::getTypeID() {
    return this->typeID;
}

void MoveMessage::setX(int32_t v) {
    this->x = v;
    this->need_recalculate_bytes = true;
}

void MoveMessage::setY(int32_t v) {
    this->y = v;
    this->need_recalculate_bytes = true;
}

void MoveMessage::setGoingX(int32_t v) {
    this->going_x = v;
    this->need_recalculate_bytes = true;
}

int32_t MoveMessage::getX() {
    return this->x;
}

int32_t MoveMessage::getY() {
    return this->y;
}

int32_t MoveMessage::getGoingX() {
    return this->going_x;
}
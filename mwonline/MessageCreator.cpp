#include "MessageCreator.h"

MessageCreator::MessageCreator() {

}

MessageCreator::~MessageCreator() {

}

std::unique_ptr<Message> MessageCreator::createMessage(
    const std::vector<uint8_t>& request
) {
    bool res;
    int32_t type_size;
    res = BitsHelper::getValue(
        request, 0, &type_size
    );

    if ( ! res) {
        std::cout << "Invalid request: type size corrupt" << std::endl;
        return nullptr;
    }
    std::string type;
    res = BitsHelper::getValue(
        request, 4, type_size, &type
    );

    if ( ! res) {
        std::cout << "Invalid request: type corrupt" << std::endl;
        return nullptr;
    }

    std::unique_ptr<Message> message = nullptr;
    if (type == LoginMessage::typeID) {
        message = std::make_unique<LoginMessage>();
        message->createFromBytes(request);
    } else if (type == MoveMessage::typeID) {
        message = std::make_unique<MoveMessage>();
        message->createFromBytes(request);
    } else if (type == StateMessage::typeID) {
        message = std::make_unique<StateMessage>();
        message->createFromBytes(request);
    } else {
        std::cout << "Not supported type request" << std::endl;
        return nullptr;
    }

    return std::move(message);
}

#include "StateMessage.h"

const std::string StateMessage::typeID = "STATE";

StateMessage::StateMessage() {

}

void StateMessage::createFromBytes(const std::vector<uint8_t>& request) {

    this->players.clear();
    this->entities.clear();

    int32_t id = 4 + this->getTypeID().length();

    int32_t players_size;
    BitsHelper::getValue(
        request, id, &players_size
    );
    id += 4;

    for (int32_t i = 0; i < players_size; i++) {
        std::unique_ptr<PlayerData> player = std::make_unique<PlayerData>();

        int32_t username_length;
        BitsHelper::getValue(
            request, id, &username_length
        );
        id += 4;

        BitsHelper::getValue(
            request, id, username_length, &player->username
        );
        id += username_length;

        int32_t texture_path_length;
        BitsHelper::getValue(
            request, id, &texture_path_length
        );
        id += 4;

        BitsHelper::getValue(
            request, id, texture_path_length, &player->texture_path
        );
        id += texture_path_length;

        BitsHelper::getValue(
            request, id, &player->x
        );
        id += 4;
        BitsHelper::getValue(
            request, id, &player->y
        );
        id += 4;
        BitsHelper::getValue(
            request, id, &player->width
        );
        id += 4;
        BitsHelper::getValue(
            request, id, &player->height
        );
        id += 4;
        BitsHelper::getValue(
            request, id, &player->going_x
        );
        id += 4;
        BitsHelper::getValue(
            request, id, &player->tex_x
        );
        id += 4;
        BitsHelper::getValue(
            request, id, &player->tex_y
        );
        id += 4;
        BitsHelper::getValue(
            request, id, &player->tex_width
        );
        id += 4;
        BitsHelper::getValue(
            request, id, &player->tex_height
        );
        id += 4;

        this->players.push_back(std::move(player));
    }

    int32_t entities_size;
    BitsHelper::getValue(
        request, id, &entities_size
    );
    id += 4;
    
    for (int32_t i = 0; i < entities_size; i++) {
        std::unique_ptr<EntityData> entity = std::make_unique<EntityData>();
        
        int32_t name_length;
        BitsHelper::getValue(
            request, id, &name_length
        );
        id += 4;

        BitsHelper::getValue(
            request, id, name_length, &entity->name
        );
        id += name_length;

        int32_t status_length;
        BitsHelper::getValue(
            request, id, &status_length
        );
        id += 4;

        BitsHelper::getValue(
            request, id, status_length, &entity->status
        );
        id += status_length;

        this->entities.push_back(std::move(entity));
    }


    this->need_recalculate_bytes = false;
}

std::vector<uint8_t> StateMessage::getBytes() {
    if (this->need_recalculate_bytes) {

        this->data.clear();

        BitsHelper::setValue(this->getTypeID().length(), this->data);
        BitsHelper::setValue(this->getTypeID(), this->data);

        BitsHelper::setValue(this->players.size(), this->data);
        for (std::unique_ptr<PlayerData>& player_data : this->players) {
            BitsHelper::setValue(player_data->username.length(), this->data);
            BitsHelper::setValue(player_data->username, this->data);

            BitsHelper::setValue(player_data->texture_path.length(), this->data);
            BitsHelper::setValue(player_data->texture_path, this->data);

            BitsHelper::setValue(player_data->x, this->data);
            BitsHelper::setValue(player_data->y, this->data);
            BitsHelper::setValue(player_data->width, this->data);
            BitsHelper::setValue(player_data->height, this->data);
            BitsHelper::setValue(player_data->going_x, this->data);
            BitsHelper::setValue(player_data->tex_x, this->data);
            BitsHelper::setValue(player_data->tex_y, this->data);
            BitsHelper::setValue(player_data->tex_width, this->data);
            BitsHelper::setValue(player_data->tex_height, this->data);
        }

        BitsHelper::setValue(this->entities.size(), this->data);
        for (std::unique_ptr<EntityData>& entity_data : this->entities) {
            BitsHelper::setValue(entity_data->name.length(), this->data);
            BitsHelper::setValue(entity_data->name, this->data);

            BitsHelper::setValue(entity_data->status.length(), this->data);
            BitsHelper::setValue(entity_data->status, this->data);
        }
    }

    assert(this->data.size() > 0);

    return this->data;
}

std::string StateMessage::getTypeID() {
    return this->typeID;
}

void StateMessage::pushPlayerData(std::unique_ptr<PlayerData> player_data) {
    this->players.push_back(std::move(player_data));
    this->need_recalculate_bytes = true;
}

void StateMessage::pushEntityData(std::unique_ptr<EntityData> entity_data) {
    this->entities.push_back(std::move(entity_data));
    this->need_recalculate_bytes = true;
}

std::vector<std::unique_ptr<PlayerData> >* StateMessage::getPlayersData() {
    return &this->players;
}

std::vector<std::unique_ptr<EntityData> >* StateMessage::getEntitiesData() {
    return &this->entities;
}

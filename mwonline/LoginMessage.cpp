#include "LoginMessage.h"

LoginMessage::LoginMessage() {

}

const std::string LoginMessage::typeID = "LOGIN";

void LoginMessage::createFromBytes(const std::vector<uint8_t>& request) {
    int32_t size_data;
    bool res;
    res = BitsHelper::getValue(
        request,
        4 + this->getTypeID().length(),
        &size_data
    );

    if ( ! res) {
        std::cout << "Invalid Login message" << std::endl;
        return;
    }

    res = BitsHelper::getValue(
        request,
        4 + this->getTypeID().length() + 4,
        size_data,
        &this->username
    );

    if ( ! res) {
        std::cout << "Invalid Login message" << std::endl;
        return;
    }

    this->data = request;
    this->need_recalculate_bytes = false;
}

std::vector<uint8_t> LoginMessage::getBytes() {
    if (this->need_recalculate_bytes) {
        this->data.clear();
        
        BitsHelper::setValue(this->typeID.length(), this->data);
        BitsHelper::setValue(this->typeID, this->data);

        BitsHelper::setValue(this->username.length(), this->data);
        BitsHelper::setValue(this->username, this->data);

        this->need_recalculate_bytes = false;
    }

    if (this->data.size() == 0) {
        std::cout << "Login message does not have byte representation" << std::endl;
        assert(false);
    }

    return this->data;
}

std::string LoginMessage::getTypeID() {
    return this->typeID;
}

void LoginMessage::setUsername(std::wstring username) {
    this->username = username;
    this->need_recalculate_bytes = true;
}

std::wstring LoginMessage::getUsername() {
    assert(this->username.length() > 0);
    return this->username;
}

#ifndef _MESSAGE_CREATOR_H_
#define _MESSAGE_CREATOR_H_

#include "Message.h"
#include "LoginMessage.h"
#include "MoveMessage.h"
#include "StateMessage.h"
#include <memory>
#include <vector>

class MessageCreator {
public:
    ~MessageCreator();

    static std::unique_ptr<Message> createMessage(
        const std::vector<uint8_t>& request
    );

private:
    MessageCreator();
};

#endif
#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include <vector>
#include <iostream>
#include <assert.h>
#include <memory>
#include "BitsHelper.h"

class Message {
public:
    virtual void createFromBytes(const std::vector<uint8_t>& request) = 0;
    virtual std::vector<uint8_t> getBytes() = 0;
    virtual std::string getTypeID() = 0;
};

#endif
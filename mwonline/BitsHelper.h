#ifndef _BITS_H_
#define _BITS_H_

#include <vector>
#include <iostream>
#include <assert.h>

class BitsHelper {
public:
    inline static bool getValue(
        const std::vector<uint8_t>& v,
        int32_t id,
        int32_t* out
    ) {
        if (v.size() - id < 4) {
            return false;
        }

        *out =
            (v[ id ] << 24) |
            (v[ id + 1 ] << 16) |
            (v[ id + 2 ] << 8) |
            (v[ id + 3 ] << 0);

        return true;
    }

    inline static bool getValue(
        const std::vector<uint8_t>& v,
        int32_t id,
        int32_t size,
        std::string* out
    ) {
        if (v.size() - id < size) {
            return false;
        }

        *out = std::string(v.begin() + id, v.begin() + id + size);

        return true;
    }

    inline static bool getValue(
        const std::vector<uint8_t>& v,
        int32_t id,
        int32_t size,
        std::wstring* out
    ) {
        if (v.size() - id < size) {
            return false;
        }

        *out = std::wstring(v.begin() + id, v.begin() + id + size);

        return true;
    }

    inline static bool setValue(
        int32_t v,
        std::vector<uint8_t>& out
    ) {
        out.push_back((v >> 24) & 0xFF);
        out.push_back((v >> 16) & 0xFF);
        out.push_back((v >> 8) & 0xFF);
        out.push_back((v >> 0) & 0xFF);

        return true;
    }

    inline static bool setValue(
        std::string v,
        std::vector<uint8_t>& out
    ) {
        out.insert(
            out.end(),
            v.begin(),
            v.end()
        );

        return true;
    }

    inline static bool setValue(
        std::wstring v,
        std::vector<uint8_t>& out
    ) {
        out.insert(
            out.end(),
            v.begin(),
            v.end()
        );

        return true;
    }

private:
    BitsHelper() {}
};

#endif
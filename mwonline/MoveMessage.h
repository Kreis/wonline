#ifndef _MOVE_MESSAGE_H_
#define _MOVE_MESSAGE_H_

#include "Message.h"

class MoveMessage : public Message {    
public:
    MoveMessage();

    void createFromBytes(const std::vector<uint8_t>& request) override;
    std::vector<uint8_t> getBytes() override;
    std::string getTypeID() override;

    void setX(int32_t v);
    void setY(int32_t v);
    void setGoingX(int32_t v);

    int32_t getX();
    int32_t getY();
    int32_t getGoingX();

    const static std::string typeID;
private:
    int32_t x;
    int32_t y;
    int32_t going_x;
    std::vector<uint8_t> data;
    bool need_recalculate_bytes = false;
};

#endif
